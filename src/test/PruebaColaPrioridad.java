package test;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

import com.opencsv.CSVReader;

import model.data_structures.ColaP;
import model.data_structures.MaxHeapCP;
import model.data_structures.Stack;
import model.vo.PruebaVO;
import model.vo.VOBike;
import model.vo.VOTrip;

public class PruebaColaPrioridad {

	private Stack<PruebaVO> pruebaStack; 
	private MaxHeapCP<PruebaVO> pruebaHeap;
	private ColaP<PruebaVO> pruebaCola;
	
	public PruebaColaPrioridad(){
		pruebaStack = new Stack<>();
	}
	public void setUp1(){
		pruebaStack = new Stack<>();
		try {
			CSVReader reader = new CSVReader(new FileReader("./data/Divvy_Trips_2017_Q3.csv"));
			String [] line = reader.readNext();
			while((line = reader.readNext()) != null){
				
					PruebaVO trip = new PruebaVO(Integer.parseInt(line[4]), //Duration
												line[6], //FromStationName 
												line[8] //ToStationName
												); 
					pruebaStack.push(trip);
				
			}
			reader.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setUp2(){
		try {
			CSVReader reader = new CSVReader(new FileReader("./data/Divvy_Trips_2017_Q4.csv"));
			String [] line = reader.readNext();
			while((line = reader.readNext()) != null){
				
					PruebaVO trip = new PruebaVO(Integer.parseInt(line[4]), //Duration
												line[6], //FromStationName 
												line[8] //ToStationName
												); 
					pruebaStack.push(trip);
				
			}
			reader.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setup3(){
		PruebaVO vo1 = new PruebaVO(137, "as", "df");
		PruebaVO vo2 = new PruebaVO(155, "as", "ss");
		PruebaVO vo3 = new PruebaVO(155, "as", "s4");
		PruebaVO vo4 = new PruebaVO(155, "as", "s2");
		PruebaVO vo5 = new PruebaVO(155, "as", "ss");
		PruebaVO vo6 = new PruebaVO(155, "as", "sq");
		PruebaVO vo7 = new PruebaVO(155, "as", "s2");
		PruebaVO vo8 = new PruebaVO(155, "as", "r5");
		pruebaHeap = new MaxHeapCP<>(8);
		pruebaCola = new ColaP<>(8);
		try{
			pruebaHeap.agregarElemento(vo1); pruebaCola.agregarElemento(vo1);
			pruebaHeap.agregarElemento(vo3); pruebaCola.agregarElemento(vo3);
			pruebaHeap.agregarElemento(vo5); pruebaCola.agregarElemento(vo5);
			pruebaHeap.agregarElemento(vo7); pruebaCola.agregarElemento(vo7);
			pruebaHeap.agregarElemento(vo8); pruebaCola.agregarElemento(vo8);
			pruebaHeap.agregarElemento(vo2); pruebaCola.agregarElemento(vo2);
			pruebaHeap.agregarElemento(vo4); pruebaCola.agregarElemento(vo4);
			pruebaHeap.agregarElemento(vo6); pruebaCola.agregarElemento(vo6);
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		
	}
	
	public long addTestHeap(int n){
		MaxHeapCP<PruebaVO> heap  = new MaxHeapCP<>(n);
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < n; i++){
			try{
				heap.agregarElemento(pruebaStack.pop());
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		long endTime = System.currentTimeMillis();
		return endTime - startTime;
	}
	
	public long maxTestHeap(int n){
		MaxHeapCP<PruebaVO> heap  = addHeap(n);
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < n; i++){
			heap.max();
		}
		long endTime = System.currentTimeMillis();
		return endTime - startTime;
	}
	
	private MaxHeapCP<PruebaVO> addHeap(int n){
		MaxHeapCP<PruebaVO> heap  = new MaxHeapCP<>(n);
		for(int i = 0; i < n; i++){
			try{
				heap.agregarElemento(pruebaStack.pop());
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return heap;
	}
	@Test
	public void TestSize(){
		setup3();
		assertTrue("Deberia ser 8", pruebaHeap.darNumeroElementos() == 8);
		assertTrue("Deberia ser 8", pruebaCola.darNumeroElementos() == 8);
		
	}
	@Test
	public void TestMax(){
		setup3();
		assertTrue(pruebaHeap.max().darEstacionFinal().equals("df"));
		assertTrue(pruebaCola.max().darEstacionFinal().equals("df"));
		
	}
	
	private ColaP<PruebaVO> addCola(int n){
		ColaP<PruebaVO> cola  = new ColaP<>(n);
		for(int i = 0; i < n; i++){
			try{
				cola.agregarElemento(pruebaStack.pop());
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return cola;
	}

	public long addTestCola(int num1) {
		ColaP<PruebaVO> cola = new ColaP<>(num1);
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < num1; i++){
			try{
				cola.agregarElemento(pruebaStack.pop());
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		long endTime = System.currentTimeMillis();
		return endTime - startTime;
	}

	public long maxTestCola(int num3) {
		ColaP<PruebaVO> heap  = addCola(num3);
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < num3; i++){
			heap.max();
		}
		long endTime = System.currentTimeMillis();
		return endTime - startTime;
	}
}
