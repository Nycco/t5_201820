package view;

import java.time.LocalDateTime;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.ColaP;
import model.data_structures.MaxHeapCP;
import model.vo.VOBike;
import test.PruebaColaPrioridad;



public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		PruebaColaPrioridad prueba = new PruebaColaPrioridad();
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					Controller.loadTrips();
					Controller.loadStations();
					
					break;
				
				case 2:
					prueba.setUp1();
					prueba.setUp2();
					break;
				case 3:
					System.out.println("Ingrese el numero de muestras de viajes:");
					int n = Integer.parseInt(sc.next());	
					try{
						int tripsLeft = Controller.generateSample(n);
						System.out.printf("Se genero un arreglo de %d viajes.\nQuedan %d viajes en el Stack\n  ", n, tripsLeft);
						break;
					}
					catch(Exception e){
						e.getMessage();
						break;
					}
					
				case 4:
					//Fecha Inicial
					System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
					String fechaInicialReq = sc.next();
					
					//Hora inicial
					System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
					String horaInicialReq = sc.next();
					
					// Datos Fecha/Hora inicial
					LocalDateTime localDateInicio = convertirFecha_Hora_LDT(fechaInicialReq, horaInicialReq);
					
					//fecha final
					System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
					String fechaFinalReq = sc.next();
					
					//hora final
					System.out.println("Ingrese la hora final (Ej: 14:25:30)");
					String horaFinalReq = sc.next();
					
					// Datos Fecha/Hora final
					LocalDateTime localDateFin = convertirFecha_Hora_LDT(fechaFinalReq, horaFinalReq);
					
					MaxHeapCP<VOBike> heap = Controller.crearMonticuloCP(localDateInicio, localDateFin);
					System.out.println("El heap se cargo con " + heap.darNumeroElementos() + " bicicletas");
					break;
					
				case 5:
					//Fecha Inicial
					System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
					String fechaInicialReq1 = sc.next();
					
					//Hora inicial
					System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
					String horaInicialReq1 = sc.next();
					
					// Datos Fecha/Hora inicial
					LocalDateTime localDateInicio1 = convertirFecha_Hora_LDT(fechaInicialReq1, horaInicialReq1);
					
					//fecha final
					System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
					String fechaFinalReq1 = sc.next();
					
					//hora final
					System.out.println("Ingrese la hora final (Ej: 14:25:30)");
					String horaFinalReq1 = sc.next();
					
					// Datos Fecha/Hora final
					LocalDateTime localDateFin1 = convertirFecha_Hora_LDT(fechaFinalReq1, horaFinalReq1);
					
					 ColaP<VOBike> cola = Controller.crearColaP(localDateInicio1, localDateFin1);
					System.out.println("La cola se cargo con " + cola.darNumeroElementos() + " bicicletas");
					break;
					
				case 6:	
					System.out.println("Ingrese el numero de elementos que quiere agregar");
					int num = Integer.parseInt(sc.next());
					long duration = prueba.addTestHeap(num);
					System.out.println("Agregar se demoro " + duration /1000.0 + " segundos");
					break;
				case 7:
					 System.out.println("Ingrese el numero de elementos que quiere agregar");
					int num1 = Integer.parseInt(sc.next());
					long duration1 = prueba.addTestCola(num1);
					System.out.println("Agregar se demoro " + duration1/1000.0+ " segundos");
					break;
				case 8:
					System.out.println("Ingrese el numero de elementos que se quiere sacar");
					int num2 = Integer.parseInt(sc.next());
					long duration2 = prueba.maxTestHeap(num2);
					System.out.println("Agregar se demoro " + duration2/1000.0 + " segundos");
					break;
				case 9:
					System.out.println("Ingrese el numero de elementos que se quiere sacar");
					int num3 = Integer.parseInt(sc.next());
					long duration3 = prueba.maxTestCola(num3);
					System.out.println("Agregar se demoro " + duration3/1000.0 + " segundos");
					break;
				case 10:
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 5----------------------");
		System.out.println("1. Cree una nueva coleccion de viajes");
		System.out.println("2. Cree una coleccion de Pruebas de viajes");
		System.out.println("3. Crear una muestra de viajes");
		System.out.println("4. crearMonticuloCP");
		System.out.println("5. crearColaCP");
		System.out.println("6. Agregar n elementos al Heap (SOLO PARA PRUEBA)" );
		System.out.println("7. Agregar n elementos a la cola (SOLO PARA PRUEBA)");
		System.out.println("8. Sacar el maximo elemento del Heap n veces (SOLO PARA PRUEBA)");
		System.out.println("9. Sacar el maximo elemento de la cola n veces (SOLO PARA PRUEBA)");
		System.out.println("10. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[1]);
		int dia = Integer.parseInt(datosFecha[0]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}
}
