package api;

import java.time.LocalDateTime;

import model.data_structures.ColaP;
import model.data_structures.MaxHeapCP;
import model.vo.VOBike;
import model.vo.VOTrip;

public interface IManejadorBicicleta {

	int generateSample(int n);
	
	void loadTrips(String tripsFile);
	
	MaxHeapCP<VOBike> crearMonticuloCP (LocalDateTime fInicial, LocalDateTime fFinal);
	
	ColaP<VOBike> crearColaP (LocalDateTime fInicial, LocalDateTime fFinal);
	
	void loadStations(String stationsFile);
	
	Comparable<VOTrip>[] getArr();
	
}
