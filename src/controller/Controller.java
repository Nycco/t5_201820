package controller;

import java.time.LocalDateTime;

import api.IManejadorBicicleta;
import model.data_structures.ColaP;
import model.data_structures.MaxHeapCP;
import model.logic.ManejadorBicicletas;
import model.vo.VOBike;
import model.vo.VOTrip;

public class Controller {

	
	private static IManejadorBicicleta manager = new ManejadorBicicletas();
	
	public static void loadTrips(){
		try {
			manager.loadTrips("./data/Divvy_Trips_2017_Q3.csv");
			manager.loadTrips("./data/Divvy_Trips_2017_Q4.csv");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static int generateSample(int n) throws Exception {
		return manager.generateSample(n);
	}
	
	public static MaxHeapCP<VOBike> crearMonticuloCP (LocalDateTime fInicial, LocalDateTime fFinal){
			return manager.crearMonticuloCP(fInicial, fFinal);
	}
	
	public static ColaP<VOBike> crearColaP(LocalDateTime fInicial, LocalDateTime fFinal){
		return manager.crearColaP(fInicial, fFinal);
	}
	
	public static void loadStations(){
		try{
			manager.loadStations("./data/Divvy_Stations_2017_Q3Q4.csv");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
