package model.data_structures;

public interface IMaxHeapCP<T> extends Iterable<T>{
	/**
	 * 
	 * @return n�mero de elementos presentes en la cola de prioridad
	 */
	int darNumeroElementos();
	/**
	 * Agrega un elemento a la cola. Se genera Exception en el caso que se sobrepase el tama�o m�ximo de la cola
	 * @param elemento
	 */
	void agregarElemento(T elemento) throws Exception;
	/**
	 * Saca/atiende el elemento m�ximo en la cola y lo retorna
	 * @return El elemento m�ximo en la cola; null en caso de que la cola este vac�a.
	 */
	T max();
	/**
	 * Retorna si la cola est� vac�a o no
	 * @return True si no tiene elementos. False en lo contrario.
	 */
	boolean esVacia();
	/**
	 * Retorna la capacidad m�xima de la cola
	 * @return la capacidad m�xoma de la cola.
	 */
	int tamanoMax();
	

}
