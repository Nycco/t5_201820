package model.data_structures;

import java.util.Iterator;

public class ColaP<T extends Comparable<T>> implements IMaxHeapCP<T>{

	
	private T[] keys;
	private int n;
	private int max;
	
	
	public ColaP(int capacidad) {
		keys = (T[]) new Comparable[capacidad];
		n = 0;
		max = capacidad;
	}


	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {
			
			int n = 0;
			@Override
			public boolean hasNext() {
				return keys[n] != null;
			}

			@Override
			public T next() {
				return keys[n++];
			}
		};
	}


	@Override
	public int darNumeroElementos() {
		return n;
	}


	@Override
	public void agregarElemento(T elemento) throws Exception {
		if(n == max) throw new Exception("La cola esta llena");
		int i = n-1;
        while (i >= 0 && elemento.compareTo(keys[i]) < 0) {
            keys[i+1] = keys[i];
            i--;
        }
        keys[i+1] = elemento;
        n++;
	}


	@Override
	public T max() {
		return keys[--n];
	}


	@Override
	public boolean esVacia() {
		// TODO Auto-generated method stub
		return n == 0;
	}


	@Override
	public int tamanoMax() {
		// TODO Auto-generated method stub
		return max;
	}
	
	

}
