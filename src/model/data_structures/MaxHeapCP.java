package model.data_structures;

import java.util.Iterator;

public class MaxHeapCP<T extends Comparable<T>> implements IMaxHeapCP<T> {

	private T[] keys;
	private int n, max;

	
	@SuppressWarnings("unchecked")
	public MaxHeapCP(int cantidad) {
		keys = (T[]) new Comparable[cantidad+1];
		max = cantidad;
		n = 0;
	}

	@Override
	public int darNumeroElementos() {
		return n;
	}

	@Override
	public void agregarElemento(T elemento) throws Exception {
		if(n == max) throw new Exception("La cola ya esta llena");
		keys[++n] = elemento;
		swim(n);
		
	}
	
	private void swim(int k){
		while(k > 1 && keys[k/2].compareTo(keys[k]) < 0){
			T temp = keys[k/2];
			keys[k/2] = keys[k];
			keys[k] = temp;
			k /= 2;
		}
	}
	
	private void sink(int k){
		while(2*k <= n ){
			int j = 2*k;
			if(j < n && keys[j].compareTo(keys[j+1]) < 0) j++;
			if(keys[k].compareTo(keys[j]) < 0){
				T temp = keys[k];
				keys[k] = keys[j];
				keys[j] = temp;
				k = j;
			}
			else break;
		}
	}

	@Override
	public T max() {
		if(esVacia()) return null;
		T max = keys[1];
		keys[1] = keys[n];
		keys[n--] = max;
		sink(1);
		keys[n+1] = null;
		return max; 
	}

	@Override
	public boolean esVacia() {
		return n == 0;
	}

	@Override
	public int tamanoMax() {
		return max;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			int m = 1;
			
			@Override
			public boolean hasNext() {
				return keys[m] != null;
			}

			@Override
			public T next() {
				return keys[m++];
			}
		};
	}

}
