package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IManejadorBicicleta;
import model.data_structures.ColaP;
import model.data_structures.MaxHeapCP;
import model.data_structures.Stack;
import model.vo.VOBike;
import model.vo.VOStation;
import model.vo.VOTrip;

public class ManejadorBicicletas implements IManejadorBicicleta{

	private Stack<VOTrip> tripStack = new Stack<>();
	private Stack<VOBike> stackBikes = new Stack<>();
	private Stack<VOStation> stackStation  = new Stack<>();
	private Comparable<VOTrip>[] arr;
	@Override
	public void loadTrips (String tripsFile) {
		try {
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String [] line = reader.readNext();
			String user = null;
			int birthyear = 0;
			while((line = reader.readNext()) != null){
				user = line[9];
				birthyear = (line[11].equals(""))? 0 : Integer.parseInt(line[11]);
				if(user.toLowerCase().equals("subscriber")){
					
					
					VOTrip trip = new VOTrip(Integer.parseInt(line[0]), //TripId
											line[1], //StartTime
											line[2], //EndTime
											Integer.parseInt(line[3]), //BikeId 
											Integer.parseInt(line[4]), //Duracion
											Integer.parseInt(line[5]), //FromStationId
											line[6], //FromStationName 
											Integer.parseInt(line[7]), //ToStationId
											line[8], //ToStationId
											line[9], //User
											line[10], //Gender
											birthyear); //Year that person was born
					tripStack.push(trip);
				}
				else if(user.toLowerCase().equals("customer")){
					
					VOTrip trip = new VOTrip(Integer.parseInt(line[0]), 
											line[1], 
											line[2], 
											Integer.parseInt(line[3]), 
											Integer.parseInt(line[4]),
											Integer.parseInt(line[5]), 
											line[6], 
											Integer.parseInt(line[7]), 
											line[8], 
											line[9]);
					tripStack.push(trip);
				}
			}
			reader.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void loadStations (String stationsFile) {
		try {
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String [] line = reader.readNext();
			int id, capacity; double latitude, longitude; String name, city, date; VOStation station;
			while((line = reader.readNext()) != null){
				id = Integer.parseInt(line[0]);
				name = line[1];
				city = line[2];
				latitude = Double.parseDouble(line[3]);
				longitude = Double.parseDouble(line[4]);
				capacity = Integer.parseInt(line[5]);
				date = line[6];
				station = new VOStation(id, name, city, latitude, longitude, capacity, date);
				stackStation.push(station);
			}
		    reader.close();

			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public int generateSample(int n) {
		arr = new VOTrip[n];
		for(int i = 0; i < n ; i++){
			arr[i] = tripStack.pop();
			
		}
		return tripStack.getSize();
		
	}
	
	public Comparable<VOTrip>[] getArr(){
		return arr;
	}
	
	public MaxHeapCP<VOBike> crearMonticuloCP(LocalDateTime fInicial, LocalDateTime fFinal) {
		MaxHeapCP<VOBike> heap = new MaxHeapCP<>(tripStack.getSize());
		Iterator<VOTrip> iter = tripStack.iterator();
		VOTrip trip; VOBike bike; VOStation fS; VOStation tS;
		while(iter.hasNext()){
			trip = iter.next();
			bike = searchBikeId(heap, trip.getBikeId());
			fS = searchStationId(trip.getFromStationId());
			tS = searchStationId(trip.getToStationId());
			if(bike != null && trip.inRange(fInicial, fFinal)){
				bike.addDistance(Haversine.distance(fS.getLatitude(), fS.getLongitude(), tS.getLatitude(), tS.getLongitude()));
			}
			else if(bike == null && trip.inRange(fInicial, fFinal)){
				try{
					heap.agregarElemento(new VOBike(trip.getBikeId(), Haversine.distance(fS.getLatitude(), fS.getLongitude(), tS.getLatitude(), tS.getLongitude())));
				}
				catch(Exception e){
					System.out.println(e.getMessage());
					return heap;
				}
			}
		}
		
		return heap;
	}

	public ColaP<VOBike> crearColaP(LocalDateTime fInicial, LocalDateTime fFinal) {
		ColaP<VOBike> cola = new ColaP<>(tripStack.getSize());
		Iterator<VOTrip> iter = tripStack.iterator();
		VOTrip trip; VOBike bike; VOStation fS; VOStation tS;
		while(iter.hasNext()){
			trip = iter.next();
			bike = searchBikeId(cola, trip.getBikeId());
			fS = searchStationId(trip.getFromStationId());
			tS = searchStationId(trip.getToStationId());
			if(bike != null && trip.inRange(fInicial, fFinal)){
				bike.addDistance(Haversine.distance(fS.getLatitude(), fS.getLongitude(), tS.getLatitude(), tS.getLongitude()));
			}
			else if(bike == null && trip.inRange(fInicial, fFinal)){
				try{
					cola.agregarElemento(new VOBike(trip.getBikeId(), Haversine.distance(fS.getLatitude(), fS.getLongitude(), tS.getLatitude(), tS.getLongitude())));
				}
				catch(Exception e){
					System.out.println(e.getMessage());
					return cola;
				}
			}
		}
		return cola;
	}
	
	
	private VOBike searchBikeId(MaxHeapCP<VOBike> heap, int id){
		for(VOBike bike: heap){
			if(bike.getBikeId()==id) return bike;
		}
		return null;
	}
	
	private VOBike searchBikeId(ColaP<VOBike> cola, int id){
		for(VOBike bike: cola){
			if(bike.getBikeId()==id) return bike;
		}
		return null;
	}
	
	private VOStation searchStationId(int id){
		VOStation station = null;
		Iterator<VOStation> iter = stackStation.iterator();
		while(iter.hasNext()){
			station = iter.next();
			if(station.getId() == id) break;
		}
		return station;
	}


	
}
