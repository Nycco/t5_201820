package model.vo;

/**
 * Representation of a byke object
 */
public class VOBike implements Comparable<VOBike>{

	
	private final int bikeId;
	private double totalDistance;

	public VOBike(int bikeId, double totalDistance) {
		this.bikeId = bikeId;
		this.totalDistance = totalDistance;
	}

	@Override
	public int compareTo(VOBike that) {
		if(this.totalDistance - that.totalDistance > 0) return 1;
		else if(this.totalDistance - that.totalDistance < 0 ) return -1;
		else return 0;
	}
	
	
	
	public int getBikeId() {
		return bikeId;
	}


	


	public double getTotalDistance() {
		return totalDistance;
	}
	
	public void addDistance(double dist){
		totalDistance += dist;
	}
	

	
}
