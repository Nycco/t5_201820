package model.vo;

public class PruebaVO implements Comparable<PruebaVO> {

	private String estacionDeInicio;
	
	private String estacionFinal;
	
	private int duracion;
	
	public PruebaVO(int duracion, String estacionDeInicio, String estacionFinal) {
		this.duracion = duracion;
		this.estacionDeInicio = estacionDeInicio;
		this.estacionFinal = estacionFinal;
	}

	@Override
	public int compareTo(PruebaVO that) {
		// TODO Auto-generated method stub
		int c = this.estacionDeInicio.compareToIgnoreCase(that.estacionDeInicio);
		if(c == 0){
			c = this.estacionFinal.compareToIgnoreCase(that.estacionFinal);
			if( c == 0) return this.duracion - that.duracion;
		}
		return -1*c;
	}
	
	public int darDuracion(){
		return duracion;
	}
	
	public String darEstacionFinal(){
		return estacionFinal;
		
	}
	
	public String darEstacionDeInicio(){
		return estacionDeInicio;
	}
}
