package model.vo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class VOStation implements Comparable<VOStation> {
	
	private int id;
	
	private String name;
	
	private String city;
	
	private double latitude, longitude;
	
	private int capacity;
	
	private LocalDateTime date;
	public VOStation(int id, String name, String city, double latitude, double longitude, int capacity, String date) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.capacity = capacity;
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("M[M]/d[d]/yyyy H[H]:mm[:ss]");
		this.date = LocalDateTime.parse(date, dateTimeFormatter);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(String date) {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
		this.date = LocalDateTime.parse(date, dateTimeFormatter);
	}
	
	@Override
	public int compareTo(VOStation that) {
		return this.date.compareTo(that.date);
	}
	
	

}
