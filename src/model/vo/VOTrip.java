package model.vo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.Locale;

import com.opencsv.bean.CsvBindByName;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip> {
	private int tripId; 
	
	private LocalDateTime startTime;
	
	private LocalDateTime endTime;
	
	private int bikeId;
	private int tripDuration;
	private int fromStationId;
	private String fromStationName;
	private int toStationId;
	private String toStationName;
	private String usertype;
	private String gender;
	private int birthyear;
	
	
	public VOTrip(int tripId, String startTime, String endTime, int bikeId, int tripDuration, int fromStationId, String fromStationName, int toStationId, String toStationName, String usertype, String gender, int birthyear){
		
		this.tripId = tripId;
		
		DateTimeFormatter format = DateTimeFormatter.ofPattern("M[M]/d[d]/yyyy H[H]:mm[:ss]");
		this.startTime = LocalDateTime.parse(startTime, format);
		this.endTime = LocalDateTime.parse(endTime, format);

		
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.fromStationId = fromStationId;
		this.fromStationName = fromStationName;
		this.toStationId = toStationId;
		this.toStationName = toStationName;
		this.usertype = usertype;
		this.gender = gender;
		this.birthyear = birthyear;
	}
	
	public VOTrip(int tripId, String startTime, String endTime, int bikeId, int tripDuration, int fromStationId, String fromStationName, int toStationId, String toStationName, String usertype){
		this(tripId, startTime, endTime, bikeId, tripDuration, fromStationId, fromStationName, toStationId, toStationName, usertype, "unknown", 0);
	}
	/**
	 * @return id - Trip_id
	 */
	public int getTripId() {
		return tripId;
	}	
	
	public int getBikeId(){
		return bikeId;
	}
	
	public int getFromStationId(){
		return fromStationId;
	}
	public int getToStationId() {
		return toStationId;
	}
	
	public LocalDateTime getStartTime(){
		return startTime;
	}
	
	public LocalDateTime getEndTime(){
		return endTime;
	}
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return tripDuration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		return fromStationName;
	}
	
	public boolean inRange(LocalDateTime fechaInicial, LocalDateTime fechaFinal){
		return startTime.isAfter(fechaInicial) && endTime.isBefore(fechaFinal);
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		return toStationName;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String toString() {
		return tripId + usertype;
	}

	@Override
	public int compareTo(VOTrip that) {
		// TODO Auto-generated method stub
		return this.startTime.compareTo(that.startTime);
	}
}
